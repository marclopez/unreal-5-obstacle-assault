# Unreal 5 Obstacle Assault

Small level created with Unreal Engine 5 using some free assets for the "Unreal 5.0 C++ Developer: Learn C++ and Make Video Games" course from GameDev.tv.
Using the "Unreal Learning Kit" and "Stylized Character Kit: Casual 01" free assets from the Unreal Engine Marketplace.
