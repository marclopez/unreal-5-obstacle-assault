// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlatform.h"

// Sets default values
AMovingPlatform::AMovingPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
	StartLocation = GetActorLocation();
}

// Called every frame
void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MovePlatform(DeltaTime);
	RotatePlatform(DeltaTime);
}

void AMovingPlatform::MovePlatform(float dT){
	// Move platform forwards
		// Get current location
	FVector CurrentLocation = GetActorLocation();
		// Add vector to that location
	CurrentLocation += PlatformVelocity * dT;
		
	// Send platform back IF gone too far
	if (ShouldPlatformReturn(CurrentLocation)){
		// get the direction normalized, 1 or -1
		FVector MoveDirection = PlatformVelocity.GetSafeNormal();
		// set start location as it can pass through if the velocity number is too big
		// places startlocation on the max move distance
		// example: 0 + 1 * 100 = 100 
		StartLocation = StartLocation + MoveDirection * MoveDistance;
		SetActorLocation(StartLocation);
		// Reverse direction
		PlatformVelocity = -PlatformVelocity;
	} else {
		// Set location
		SetActorLocation(CurrentLocation);
	}
}

void AMovingPlatform::RotatePlatform(float dT){
	AddActorLocalRotation(RotationVelocity * dT);
}

bool AMovingPlatform::ShouldPlatformReturn(FVector CurrentLocation) const {
	return GetDistanceMoved(CurrentLocation) > MoveDistance;
}

// Check how far it moved
float AMovingPlatform::GetDistanceMoved(FVector CurrentLocation) const {
	return FVector::Distance(StartLocation, CurrentLocation);
}




// some code to remember functions

/*
FString ActorName = GetName();
UE_LOG(LogTemp, Display, TEXT("Begin play... %s"), *ActorName);
*/

/*
float OverShoot = DistanceMoved - MoveDistance;
FString ActorName = GetName();
UE_LOG(LogTemp, Display, TEXT("%s over distance: %f"), *ActorName, OverShoot);
*/

